//package ru.pet.shop.vlad.fateev.starter.client;
//
//import org.springframework.cloud.openfeign.FeignClient;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import ru.pet.shop.vlad.fateev.starter.dto.AdmDto;
//import ru.pet.shop.vlad.fateev.starter.setting.AdmSetting;
//
//import java.util.List;
//
//@FeignClient(name = "ADMINISTRATIVE-SETUP-SERVICE")
//public interface AdmFeignClient {
//
//    @GetMapping("/adm/setting/by_id/{setting_id}")
//    AdmDto getSettingById(@PathVariable("setting_id") Long id);
//
//    @GetMapping("/adm/setting/by_enum/{setting}")
//    AdmDto getSettingByString(@PathVariable("setting") AdmSetting setting);
//
//    @GetMapping("/adm/settings/by_enum/{settings}")
//    List<AdmDto> getListSetting(@PathVariable("settings") List<AdmSetting> settings);
//}
