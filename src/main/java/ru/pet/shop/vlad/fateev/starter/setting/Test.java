package ru.pet.shop.vlad.fateev.starter.setting;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class Test {

    public class TestWriter implements Runnable {

        @Override
        public void run() {
            System.out.println("WRITE");
            log.info("WRITEEEEE!!!!");
        }
    }

    @Bean
    public TestWriter initTestWriter() {
        TestWriter writer = new TestWriter();
        writer.run();
        return writer;
    }
}
